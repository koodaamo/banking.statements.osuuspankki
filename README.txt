Introduction
============

This is a plugin for use with banking.statements package. It implements
reading from three different statement entry CSV formats that have been
used by Osuuspankki of Finland.

There is support for both personal and corporate CSV export formats.
